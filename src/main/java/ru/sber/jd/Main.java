package ru.sber.jd;

import ru.sber.jd.entities.Credit;
import ru.sber.jd.entities.Fot;
import ru.sber.jd.entities.JoinTables;
import ru.sber.jd.entities.Passive;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {


        Connection connection = DbUtils.createConnetion();

        DbUtils.dropTables(connection);

        DbUtils.createTableCredit(connection);

        DbUtils.insertCredit(connection, Credit.builder()
                .gosbId(8610)
                .gosbName("Банк Татарстан")
                .tbName("ВВБ")
                .segment("ММБ")
                .creditValue(8000d)
                .build());
        DbUtils.insertCredit(connection, Credit.builder()
                .gosbId(8611)
                .gosbName("Владимирское ГОСБ")
                .tbName("ВВБ")
                .segment("КСБ")
                .creditValue(14000d)
                .build());
        DbUtils.insertCredit(connection, Credit.builder()
                .gosbId(8612)
                .gosbName("Кировское ГОСБ")
                .tbName("ВВБ")
                .segment("РГС")
                .creditValue(50d)
                .build());


        DbUtils.createTableFot(connection);

        DbUtils.insertFot(connection, Fot.builder()
                .gosbId(8610)
                .gosbName("Банк Татарстан")
                .tbName("ВВБ")
                .segment("ММБ")
                .fotValue(10d)
                .build());
        DbUtils.insertFot(connection, Fot.builder()
                .gosbId(8611)
                .gosbName("Владимирское ГОСБ")
                .tbName("ВВБ")
                .segment("КСБ")
                .fotValue(11d)
                .build());
        DbUtils.insertFot(connection, Fot.builder()
                .gosbId(8612)
                .gosbName("Кировское ГОСБ")
                .tbName("ВВБ")
                .segment("РГС")
                .fotValue(12d)
                .build());

        DbUtils.createTablePassive(connection);

        DbUtils.insertPassive(connection, Passive.builder()
                .gosbId(8610)
                .gosbName("Банк Татарстан")
                .tbName("ВВБ")
                .segment("ММБ")
                .passiveValue(16000d)
                .build());
        DbUtils.insertPassive(connection, Passive.builder()
                .gosbId(8611)
                .gosbName("Владимирское ГОСБ")
                .tbName("ВВБ")
                .segment("КСБ")
                .passiveValue(28000d)
                .build());
        DbUtils.insertPassive(connection, Passive.builder()
                .gosbId(8612)
                .gosbName("Кировское ГОСБ")
                .tbName("ВВБ")
                .segment("РГС")
                .passiveValue(100d)
                .build());



        System.out.println("select * credit");
        for (Credit credit : DbUtils.selectAllCredit(connection)) {
            System.out.println(credit);
        }

        System.out.println("\nselect * fot");
        for (Fot fot : DbUtils.selectAllFot(connection)) {
            System.out.println(fot);
        }

        System.out.println("\nselect * passive");
        for (Passive passive : DbUtils.selectAllPassive(connection)) {
            System.out.println(passive);
        }

        System.out.println("\njoin all tables");
        for (JoinTables joinTables : DbUtils.selectJoinAllTables(connection)) {
            System.out.println(joinTables);
        }

        System.out.println("\njoin all tables group by");
        for (JoinTables joinTables : DbUtils.selectJoinAllTablesGroupBy(connection)) {
            System.out.println(joinTables);
        }


    }


}
