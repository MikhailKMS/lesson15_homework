package ru.sber.jd.entities;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Credit {

    private Integer gosbId;
    private String gosbName;
    private String tbName;
    private String segment;
    private Double creditValue;


}