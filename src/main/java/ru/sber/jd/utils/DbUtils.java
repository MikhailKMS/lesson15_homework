package ru.sber.jd.utils;



import ru.sber.jd.entities.Credit;
import ru.sber.jd.entities.Fot;
import ru.sber.jd.entities.JoinTables;
import ru.sber.jd.entities.Passive;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbUtils {

    public static Connection createConnetion() {
        try {
            return DriverManager.getConnection("jdbc:h2:~/sql;MODE=PostgreSQL", "sa", "");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }


    public static void dropTables(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("drop table if exists credit");
        statement.execute("drop table if exists fot");
        statement.execute("drop table if exists passive");
        connection.commit();
    }

    public static void createTableCredit(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("create table if not exists credit (gosb_id integer, gosb_name text, tb_name text, segment text, credit double)");
        connection.commit();
    }

    public static void createTableFot(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("create table if not exists fot (gosb_id integer, gosb_name text, tb_name text, segment text, fot double)");
        connection.commit();
    }

    public static void createTablePassive(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("create table if not exists passive (gosb_id integer, gosb_name text, tb_name text, segment text, passive double)");
        connection.commit();
    }

    public static void insertCredit(Connection connection, Credit credit) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(String.format("insert into credit (gosb_id, gosb_name, tb_name, segment, credit)" +
                "values (%s, '%s', '%s', '%s', %s)", credit.getGosbId(), credit.getGosbName(), credit.getTbName(), credit.getSegment(), credit.getCreditValue()));
        connection.commit();
    }

    public static void insertFot(Connection connection, Fot fot) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(String.format("insert into fot (gosb_id, gosb_name, tb_name, segment, fot)" +
                "values (%s, '%s', '%s', '%s', %s)", fot.getGosbId(), fot.getGosbName(), fot.getTbName(), fot.getSegment(), fot.getFotValue()));
        connection.commit();
    }
    public static void insertPassive(Connection connection, Passive passive) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(String.format("insert into passive (gosb_id, gosb_name, tb_name, segment, passive)" +
                "values (%s, '%s', '%s', '%s', %s)", passive.getGosbId(), passive.getGosbName(), passive.getTbName(), passive.getSegment(), passive.getPassiveValue()));
        connection.commit();
    }

    public static List<Credit> selectAllCredit(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from fot");

        List<Credit> gosbs = new ArrayList<>();

        while (resultSet.next()) {
            Integer gosbID = resultSet.getInt(1);
            String gosbName = resultSet.getString(2);
            String tbName = resultSet.getString(3);
            String segment = resultSet.getString(4);
            Double creditValue = resultSet.getDouble(5);


            gosbs.add(Credit.builder()
                    .gosbId(gosbID)
                    .gosbName(gosbName)
                    .tbName(tbName)
                    .segment(segment)
                    .creditValue(creditValue)
                    .build()
            );
        }
        return gosbs;
    }


    public static List<Fot> selectAllFot(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from fot");

        List<Fot> gosbs = new ArrayList<>();

        while (resultSet.next()) {
            Integer gosbID = resultSet.getInt(1);
            String gosbName = resultSet.getString(2);
            String tbName = resultSet.getString(3);
            String segment = resultSet.getString(4);
            Double creditValue = resultSet.getDouble(5);


            gosbs.add(Fot.builder()
                    .gosbId(gosbID)
                    .gosbName(gosbName)
                    .tbName(tbName)
                    .segment(segment)
                    .fotValue(creditValue)
                    .build()
            );
        }
        return gosbs;
    }

    public static List<Passive> selectAllPassive(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from passive");

        List<Passive> gosbs = new ArrayList<>();

        while (resultSet.next()) {
            Integer gosbID = resultSet.getInt(1);
            String gosbName = resultSet.getString(2);
            String tbName = resultSet.getString(3);
            String segment = resultSet.getString(4);
            Double creditValue = resultSet.getDouble(5);


            gosbs.add(Passive.builder()
                    .gosbId(gosbID)
                    .gosbName(gosbName)
                    .tbName(tbName)
                    .segment(segment)
                    .passiveValue(creditValue)
                    .build()
            );
        }
        return gosbs;
    }


    public static List<JoinTables> selectJoinAllTables(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select credit.*, fot.fot, passive.passive from credit left join fot on fot.gosb_id=credit.gosb_id left join passive on passive.gosb_id=credit.gosb_id");

        List<JoinTables> gosbs = new ArrayList<>();

        while (resultSet.next()) {
            Integer gosbID = resultSet.getInt(1);
            String gosbName = resultSet.getString(2);
            String tbName = resultSet.getString(3);
            String segment = resultSet.getString(4);
            Double creditValue = resultSet.getDouble(5);
            Double fotValue = resultSet.getDouble(6);
            Double passiveValue = resultSet.getDouble(7);


            gosbs.add(JoinTables.builder()
                    .gosbId(gosbID)
                    .gosbName(gosbName)
                    .tbName(tbName)
                    .segment(segment)
                    .creditValue(creditValue)
                    .fotValue(fotValue)
                    .passiveValue(passiveValue)
                    .build()
            );
        }
        return gosbs;
    }

    public static List<JoinTables> selectJoinAllTablesGroupBy(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select credit.tb_name, sum(credit.credit), sum(fot.fot), sum(passive.passive) from credit left join fot on fot.gosb_id=credit.gosb_id left join passive on passive.gosb_id=credit.gosb_id group by credit.tb_name");

        List<JoinTables> gosbs = new ArrayList<>();

        while (resultSet.next()) {
            String tbName = resultSet.getString(1);
            Double creditValue = resultSet.getDouble(2);
            Double fotValue = resultSet.getDouble(3);
            Double passiveValue = resultSet.getDouble(4);


            gosbs.add(JoinTables.builder()
                    .tbName(tbName)
                    .creditValue(creditValue)
                    .fotValue(fotValue)
                    .passiveValue(passiveValue)
                    .build()
            );
        }
        return gosbs;
    }







}
